﻿CREATE TABLE [dbo].[D030_CLIENTE](
	[NroInscricao] [int] NOT NULL,
	[SeqCliente] [smallint] NOT NULL,
	[NroVinculo] [smallint] NULL,
	[NmeCliente] [char](70) NOT NULL,
	[NmeCartao] [varchar](23) NULL,
	[SglSexo] [char](1) NULL,
	[DtaNascimento] [datetime] NULL,
	[TipoInvalido] [smallint] NULL,
	[StaOpcional] [smallint] NULL,
	[NroOl] [smallint] NULL,
	[NroCidade] [int] NULL,
	[DtaInscricao] [datetime] NULL,
	[DtaCancelamento] [datetime] NULL,
	[NroObs] [smallint] NULL,
	[DtaAtualizacao] [datetime] NULL,
	[UserId] [varchar](15) NULL,
	[UserCpf] [numeric](11, 0) NULL,
	[DtaIniCarencia] [smalldatetime] NULL,
	[NroSitCarencia] [tinyint] NULL,
	[ObsSitCarencia] [varchar](60) NULL,
	[QtdCancelamento] [tinyint] NULL,
	[NmeMae] [char](70) NOT NULL,
	[StaReingresso] [tinyint] NOT NULL,
	[NroCusteio] [tinyint] NULL,
	[StaErroEndereco] [tinyint] NOT NULL,
	[StaEndTitular] [tinyint] NOT NULL,
	[DtaAdocao] [smalldatetime] NULL,
	[NroPisPasep] [numeric](11, 0) NULL,
	[NroCNS] [numeric](15, 0) NULL,
	[StaEnviaCorrespondenciaTitular] [tinyint] NOT NULL,
	[StaUrgenciaCorrespondencia] [tinyint] NOT NULL,
	[NroCpfCliente] [numeric](11, 0) NULL,
	[NroEstCivil] [tinyint] NULL,
	[NroIdentidade] [varchar](20) NULL,
	[NmeOrgaoEmissor] [char](6) NULL,
	[SglUfIdentidade] [char](2) NULL,
	[DtaEmissaoId] [datetime] NULL,
	[CodPaisIdentidade] [tinyint] NULL,
	[DtaEvento] [smalldatetime] NULL,
	[CodMatricula] [char](10) NULL,
	[StaPendenciaSIAPE] [tinyint] NOT NULL,
	[NroDecNascidoVivo] [numeric](12, 0) NULL,
	[NroRaca] [tinyint] NULL,
	[email] [varchar](64) NULL,
	[email2] [varchar](64) NULL,
 CONSTRAINT [PK_D030_CLIENTE] PRIMARY KEY NONCLUSTERED 
(
	[NroInscricao] ASC,
	[SeqCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[D030_CLIENTE] ADD  CONSTRAINT [DF_D030_CLIENTE_TipoInvalido]  DEFAULT (0) FOR [TipoInvalido]
GO
ALTER TABLE [dbo].[D030_CLIENTE] ADD  CONSTRAINT [DF_D030_CLIENTE_StaReingresso]  DEFAULT (0) FOR [StaReingresso]
GO
ALTER TABLE [dbo].[D030_CLIENTE] ADD  CONSTRAINT [DF_D030_CLIENTE_StaErroEndereco]  DEFAULT (0) FOR [StaErroEndereco]
GO
ALTER TABLE [dbo].[D030_CLIENTE] ADD  CONSTRAINT [DF_D030_CLIENTE_StaEndTitular]  DEFAULT (1) FOR [StaEndTitular]
GO
ALTER TABLE [dbo].[D030_CLIENTE] ADD  DEFAULT ((1)) FOR [StaEnviaCorrespondenciaTitular]
GO
ALTER TABLE [dbo].[D030_CLIENTE] ADD  DEFAULT ((0)) FOR [StaUrgenciaCorrespondencia]
GO
ALTER TABLE [dbo].[D030_CLIENTE] ADD  DEFAULT ((0)) FOR [StaPendenciaSIAPE]