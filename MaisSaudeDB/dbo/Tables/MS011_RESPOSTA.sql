﻿CREATE TABLE [dbo].[MS011_RESPOSTA](
	[NroResposta] [int] NOT NULL IDENTITY(1,1),
	[NroQuestao] [int] NOT NULL,
	[DesResposta] [varchar](60) NOT NULL,
	[DesOrientacao] [varchar](255) NULL,
	[StaPermiteOutro] [tinyint] NOT NULL,
	[NroOrdemResposta] [tinyint] NULL,
	[DtaIniValidade] [smalldatetime] NOT NULL,
	[DtaFimValidade] [smalldatetime] NULL,
	[DtaAtualizacao] [smalldatetime] NOT NULL,
	[UserId] [numeric](11, 0) NOT NULL,
 CONSTRAINT [PK_MS011_RESPOSTA] PRIMARY KEY CLUSTERED 
(
	[NroResposta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MS011_RESPOSTA]  WITH CHECK ADD  CONSTRAINT [FK_MS011_RESPOSTA_MS009_QUESTAO] FOREIGN KEY([NroQuestao])
REFERENCES [dbo].[MS009_QUESTAO] ([NroQuestao])
GO

ALTER TABLE [dbo].[MS011_RESPOSTA] CHECK CONSTRAINT [FK_MS011_RESPOSTA_MS009_QUESTAO]