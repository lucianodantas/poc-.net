﻿CREATE TABLE [dbo].[SS11_PERMISSAO](
	[NroPermissao] [tinyint] NOT NULL,
	[NmePermissao] [varchar](30) NOT NULL,
	[AbrPermissao] [varchar](15) NOT NULL,
	[DtaAtualizacao] [smalldatetime] NULL,
	[UserId] [numeric](11, 0) NULL,
 CONSTRAINT [PK_SS11_PERMISSAO] PRIMARY KEY CLUSTERED 
(
	[NroPermissao] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]