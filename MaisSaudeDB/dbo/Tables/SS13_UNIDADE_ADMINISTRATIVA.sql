﻿CREATE TABLE [dbo].[SS13_UNIDADE_ADMINISTRATIVA](
	[NroUa] [tinyint] NOT NULL,
	[NmeUa] [varchar](30) NOT NULL,
	[AbrUa] [varchar](15) NOT NULL,
	[DtaAtualizacao] [smalldatetime] NULL,
	[UserId] [numeric](11, 0) NULL,
	[NroUAReal] [tinyint] NOT NULL,
 CONSTRAINT [PK_SS13_UNIDADE_ADMINISTRATIVA] PRIMARY KEY CLUSTERED 
(
	[NroUa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SS13_UNIDADE_ADMINISTRATIVA] ADD  DEFAULT ((0)) FOR [NroUAReal]