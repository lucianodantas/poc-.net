﻿CREATE TABLE [dbo].[MS003_GRUPO_VERSAO](
	[NroGrupoVersao] [int] NOT NULL IDENTITY(1,1),
	[NroVersao] [int] NOT NULL,
	[NroGrupo] [int] NOT NULL,
	[NroOrdem] [int] NULL,
	[StaValorado] [tinyint] NULL,
	[DtaAtualizacao] [smalldatetime] NOT NULL,
	[UserId] [numeric](11, 0) NOT NULL,
 CONSTRAINT [PK_MS003_GRUPO_VERSAO] PRIMARY KEY CLUSTERED 
(
	[NroGrupoVersao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MS003_GRUPO_VERSAO]  WITH CHECK ADD  CONSTRAINT [FK_MS003_GRUPO_VERSAO_MS005_VERSAO] FOREIGN KEY([NroVersao])
REFERENCES [dbo].[MS005_VERSAO] ([NroVersao])
GO

ALTER TABLE [dbo].[MS003_GRUPO_VERSAO] CHECK CONSTRAINT [FK_MS003_GRUPO_VERSAO_MS005_VERSAO]
GO
ALTER TABLE [dbo].[MS003_GRUPO_VERSAO]  WITH CHECK ADD  CONSTRAINT [FK_MS003_GRUPO_VERSAO_MS059_GRUPO] FOREIGN KEY([NroGrupo])
REFERENCES [dbo].[MS059_GRUPO] ([NroGrupo])
GO

ALTER TABLE [dbo].[MS003_GRUPO_VERSAO] CHECK CONSTRAINT [FK_MS003_GRUPO_VERSAO_MS059_GRUPO]