﻿CREATE TABLE [dbo].[SS07_OBJETO](
	[NroObjeto] [int] IDENTITY(1,1) NOT NULL,
	[NmeObjeto] [varchar](60) NOT NULL,
	[DtaIniValidade] [smalldatetime] NOT NULL,
	[DtaFimValidade] [smalldatetime] NULL,
	[DtaAtualizacao] [smalldatetime] NULL,
	[UserId] [numeric](11, 0) NULL,
	[NroModulo] [int] NULL,
	[StaAtividade] [tinyint] NULL,
 CONSTRAINT [PK_SS07_OBJETO] PRIMARY KEY CLUSTERED 
(
	[NroObjeto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]