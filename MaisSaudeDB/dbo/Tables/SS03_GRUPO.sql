﻿CREATE TABLE [dbo].[SS03_GRUPO](
	[NroGrupo] [int] IDENTITY(1,1) NOT NULL,
	[NmeGrupo] [varchar](60) NOT NULL,
	[DtaIniValidade] [smalldatetime] NOT NULL,
	[DtaFimValidade] [smalldatetime] NULL,
	[DtaAtualizacao] [smalldatetime] NULL,
	[UserId] [numeric](11, 0) NULL,
 CONSTRAINT [PK_SS03_GRUPO] PRIMARY KEY CLUSTERED 
(
	[NroGrupo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]