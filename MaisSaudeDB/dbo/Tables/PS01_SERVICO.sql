﻿CREATE TABLE [dbo].[PS01_SERVICO](
	[NroServico] [int] NOT NULL,
	[NmeServico] [varchar](320) NULL,
	[NroTabServico] [tinyint] NOT NULL,
	[NroGrpServico] [tinyint] NOT NULL,
	[NroSubGrpSip] [int] NOT NULL,
	[CodPerfil] [char](1) NOT NULL,
	[DtaIniValidade] [smalldatetime] NOT NULL,
	[DtaFimValidade] [smalldatetime] NULL,
	[NroPrtAnestesico] [tinyint] NULL,
	[StaTUSS] [tinyint] NOT NULL,
	[DtaAtualizacao] [smalldatetime] NOT NULL,
	[UserId] [numeric](11, 0) NOT NULL,
	[StaRol] [tinyint] NOT NULL,
 CONSTRAINT [PK_PS01_SERVICO] PRIMARY KEY CLUSTERED 
(
	[NroServico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 98) ON [PRIMARY]
) ON [PRIMARY]
GO





ALTER TABLE [dbo].[PS01_SERVICO] ADD  DEFAULT ((0)) FOR [StaRol]