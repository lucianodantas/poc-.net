﻿CREATE TABLE [dbo].[MS061_LISTA_CORPORATIVA](
	[NroListaCorporativa] [smallint] NOT NULL IDENTITY(1,1),
	[NmeListaCorporativa] [varchar](60) NOT NULL,
	[DesListaCorporativa] [varchar](255) NULL,
	[DtaAtualizacao] [smalldatetime] NOT NULL,
	[UserId] [numeric](11, 0) NOT NULL,
 CONSTRAINT [PK_MS061_LISTA_CORPORATIVA] PRIMARY KEY CLUSTERED 
(
	[NroListaCorporativa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]