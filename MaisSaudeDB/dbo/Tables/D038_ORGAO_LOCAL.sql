﻿CREATE TABLE [dbo].[D038_ORGAO_LOCAL](
	[NroOl] [smallint] NOT NULL,
	[NmeEstado] [varchar](35) NULL,
	[SglEstado] [varchar](2) NULL,
	[NmeResponsavel] [varchar](40) NULL,
	[Cargo] [varchar](40) NULL,
	[Logradouro] [varchar](40) NULL,
	[Bairro] [varchar](20) NULL,
	[Cidade] [varchar](20) NULL,
	[Cep] [varchar](8) NULL,
	[NroCidadePadrao] [int] NULL,
	[StaOlAtual] [int] NULL,
	[StaUsoCartao] [tinyint] NULL,
	[NmeCapital] [varchar](20) NULL,
	[DtaInibicao] [datetime] NULL,
	[DtaAtualizacao] [smalldatetime] NULL,
	[UserId] [numeric](11, 0) NULL,
	[NroCGC] [numeric](14, 0) NULL,
	[NroLatitude] [float] NULL,
	[NroLongitude] [float] NULL,
 CONSTRAINT [PK_D038_ORGAO_LOCAL] PRIMARY KEY CLUSTERED 
(
	[NroOl] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]