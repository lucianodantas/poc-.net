﻿CREATE TABLE [dbo].[SS09_DIRETIVA](
	[NroObjeto] [int] NOT NULL,
	[NroGrupo] [int] NOT NULL,
	[NroPermissao] [tinyint] NOT NULL,
	[DtaAtualizacao] [smalldatetime] NULL,
	[UserId] [numeric](11, 0) NULL,
 CONSTRAINT [PK_SS09_DIRETIVA] PRIMARY KEY CLUSTERED 
(
	[NroObjeto] ASC,
	[NroGrupo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]