﻿CREATE TABLE [dbo].[MS057_AGENTE_MAIS_SAUDE](
	[NroAgenteMaisSaude] [int] NOT NULL IDENTITY(1,1),
	[NroTipoProfissional] [int] NOT NULL,
	[NroCPFProfissional] [numeric](11, 0) NOT NULL,
	[NmeProfissional] [varchar](60) NOT NULL,
	[DtaIniValidade] [smalldatetime] NOT NULL,
	[DtaFimValidade] [smalldatetime] NULL,
	[DtaAtualizacao] [smalldatetime] NOT NULL,
	[UserId] [numeric](11, 0) NOT NULL,
 CONSTRAINT [PK_MS057_AGENTE_MAIS_SAUDE] PRIMARY KEY CLUSTERED 
(
	[NroAgenteMaisSaude] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]