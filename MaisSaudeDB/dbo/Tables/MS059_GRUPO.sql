﻿CREATE TABLE [dbo].[MS059_GRUPO](
	[NroGrupo] [int] NOT NULL IDENTITY(1,1),
	[Nmegrupo] [varchar](60) NOT NULL,
	[Desgrupo] [varchar](256) NOT NULL,
	[DtaInibicao] [smalldatetime] NOT NULL,
	[DtaAtualizacao] [smalldatetime] NOT NULL,
	[UserId] [numeric](11, 0) NOT NULL,
 CONSTRAINT [PK_MS059_GRUPO] PRIMARY KEY CLUSTERED 
(
	[NroGrupo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]