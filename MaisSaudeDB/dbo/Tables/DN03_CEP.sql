﻿CREATE TABLE [dbo].[DN03_CEP](
	[NroSeqLogradouro] [int] IDENTITY(1,1) NOT NULL,
	[NroCEP] [int] NOT NULL,
	[LogrAbrv] [varchar](25) NULL,
	[Endereco] [varchar](250) NULL,
	[Complemento] [varchar](200) NULL,
	[Bairro] [varchar](150) NULL,
	[Cidade] [varchar](120) NULL,
	[UF] [varchar](2) NULL,
	[UserId] [numeric](11, 0) NULL,
	[DtaAtualizacao] [datetime] NULL,
 CONSTRAINT [PK_DN03_CEP] PRIMARY KEY CLUSTERED 
(
	[NroSeqLogradouro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 98) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DN03_CEP] ADD  DEFAULT (getdate()) FOR [DtaAtualizacao]