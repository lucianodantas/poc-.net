﻿CREATE TABLE [dbo].[SS01_USUARIO](
	[NroCpf] [numeric](11, 0) NOT NULL,
	[PrimeiroNome] [char](15) NOT NULL,
	[NomeDoMeio] [char](40) NULL,
	[UltimoNome] [char](15) NOT NULL,
	[DtaIniValidade] [smalldatetime] NOT NULL,
	[DtaFimValidade] [smalldatetime] NULL,
	[Usuario] [char](32) NOT NULL,
	[Password] [varbinary](256) NULL,
	[Password1] [varbinary](256) NULL,
	[Password2] [varbinary](256) NULL,
	[Password3] [varbinary](256) NULL,
	[DtaFimPassword] [smalldatetime] NULL,
	[NroErrLogon] [tinyint] NULL,
	[DtaErrLogon] [smalldatetime] NULL,
	[NroUa] [tinyint] NOT NULL,
	[DtaAtualizacao] [smalldatetime] NULL,
	[UserId] [numeric](11, 0) NULL,
	[NmeCompleto]  AS (((rtrim([PrimeiroNome])+case when [NomeDoMeio] IS NULL then '' else ' '+rtrim([NomeDoMeio]) end)+' ')+rtrim(isnull([UltimoNome],''))),
	[NroLotacao] [tinyint] NULL,
	[NroArea] [tinyint] NOT NULL,
	[DtaSuspensao] [smalldatetime] NULL,
	[DtaDemissao] [smalldatetime] NULL,
 CONSTRAINT [PK_SS01_USUARIO] PRIMARY KEY CLUSTERED 
(
	[NroCpf] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 98) ON [PRIMARY]
) ON [PRIMARY]