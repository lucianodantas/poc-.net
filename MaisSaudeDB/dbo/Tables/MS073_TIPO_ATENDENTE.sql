﻿CREATE TABLE [dbo].[MS073_TIPO_ATENDENTE](
	[NroTpoAtendente] [tinyint] NOT NULL IDENTITY(1,1),
	[NmeTpoAtendente] [varchar](60) NOT NULL,
	[DtaIniValidade] [smalldatetime] NOT NULL,
	[DtaFimValidade] [smalldatetime] NULL,
	[DtaAtualizacao] [smalldatetime] NOT NULL,
	[UserId] [numeric](11, 0) NOT NULL,
 CONSTRAINT [PK_MS073_TIPO_ATENDENTE] PRIMARY KEY CLUSTERED 
(
	[NroTpoAtendente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]