﻿CREATE TABLE [dbo].[MS037_SITUACAO_BENEFICIARIO](
	[NroStaBeneficiario] [tinyint] NOT NULL IDENTITY(1,1),
	[DesStaBeneficiario] [varchar](60) NOT NULL,
	[DtaAtualizacao] [smalldatetime] NOT NULL,
	[UserId] [numeric](11, 0) NOT NULL,
 CONSTRAINT [PK_MS037_SITUACAO_BENEFICIARIO] PRIMARY KEY CLUSTERED 
(
	[NroStaBeneficiario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]