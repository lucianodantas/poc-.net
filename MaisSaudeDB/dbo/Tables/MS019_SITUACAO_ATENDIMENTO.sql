﻿CREATE TABLE [dbo].[MS019_SITUACAO_ATENDIMENTO](
	[NroStaAtendimento] [smallint] NOT NULL IDENTITY(1,1),
	[DesStaAtendimento] [varchar](60) NOT NULL,
	[DtaAtualizacao] [smalldatetime] NOT NULL,
	[UserId] [numeric](11, 0) NOT NULL,
 CONSTRAINT [PK_MS019_SITUACAO_ATENDIMENTO] PRIMARY KEY CLUSTERED 
(
	[NroStaAtendimento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]