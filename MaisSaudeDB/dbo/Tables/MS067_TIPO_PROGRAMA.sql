﻿CREATE TABLE [dbo].[MS067_TIPO_PROGRAMA](
	[NroTpoPrograma] [tinyint] NOT NULL IDENTITY(1,1),
	[NmeTpoPrograma] [varchar](60) NOT NULL,
	[DesTpoPrograma] [varchar](512) NULL,
	[DtaInibicao] [smalldatetime] NULL,
	[DtaAtualizacao] [smalldatetime] NOT NULL,
	[UserId] [numeric](11, 0) NOT NULL,
 CONSTRAINT [PK_MS067_TIPO_PROGRAMA] PRIMARY KEY CLUSTERED 
(
	[NroTpoPrograma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]