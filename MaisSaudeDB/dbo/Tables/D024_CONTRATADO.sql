﻿CREATE TABLE [dbo].[D024_CONTRATADO](
	[NroContratado] [int] NOT NULL,
	[NroOl] [smallint] NULL,
	[CodCredenciado] [varchar](5) NULL,
	[DvCredenciado] [varchar](1) NULL,
	[Natureza] [varchar](1) NULL,
	[NmeContratado] [varchar](70) NULL,
	[PessoaFisJur] [varchar](1) NULL,
	[StaDescontaIr] [tinyint] NULL,
	[PctAliquotaIr] [money] NULL,
	[NroCodRetencaoIr] [smallint] NULL,
	[NroClasse] [smallint] NULL,
	[TipoCooperativa] [tinyint] NULL,
	[StaDescontaPensao] [tinyint] NOT NULL,
	[StaHoraExtra] [tinyint] NOT NULL,
	[CodCgcCpf] [varchar](9) NULL,
	[CodFilialCgc] [varchar](4) NULL,
	[DvCgcCpf] [varchar](2) NULL,
	[TpoConta] [tinyint] NULL,
	[NroBanco] [smallint] NULL,
	[NroAgencia] [smallint] NULL,
	[DvAgencia] [varchar](1) NULL,
	[NroContaCorrente] [int] NULL,
	[DvContaCorrente] [varchar](1) NULL,
	[DtaInscricao] [datetime] NULL,
	[DtaCancelamento] [datetime] NULL,
	[NroObs] [smallint] NULL,
	[DtaUltAtendimento] [datetime] NULL,
	[Contrato] [varchar](10) NULL,
	[CodConselho] [varchar](5) NULL,
	[CodInsConselho] [varchar](10) NULL,
	[Logradouro] [varchar](40) NULL,
	[Complemento] [varchar](40) NULL,
	[Bairro] [varchar](20) NULL,
	[Cep] [int] NULL,
	[NroCidade] [int] NULL,
	[Cidade] [varchar](20) NULL,
	[Uf] [varchar](2) NULL,
	[DtaAtualizacao] [datetime] NULL,
	[UserId] [varchar](32) NULL,
	[NroContratadoNovo] [int] NULL,
	[StaPGC] [tinyint] NOT NULL,
	[StaSuspeito] [tinyint] NULL,
	[email] [varchar](64) NULL,
	[EmailFat] [varchar](64) NULL,
	[NmeFantasia] [varchar](70) NULL,
	[StaReferenciado] [tinyint] NULL,
	[StaPerCapta] [tinyint] NOT NULL,
	[NroTelefone] [int] NULL,
	[NroDDD] [tinyint] NULL,
	[NmePontoReferencia] [varchar](30) NULL,
	[NroFax] [int] NULL,
	[NroCelular] [int] NULL,
	[UFConselho] [tinyint] NULL,
	[txtObsCancelamento] [varchar](200) NULL,
	[Endereco] [varchar](60) NULL,
	[StaEmite2aOpiniao] [tinyint] NOT NULL,
	[StaPrgCabecaFresca] [smallint] NOT NULL,
	[StaAcessoLeilao] [tinyint] NOT NULL,
	[StaUsoMatMedEletronico] [tinyint] NOT NULL,
	[StaCentralizaAuditoria] [tinyint] NOT NULL,
	[EndSite] [varchar](70) NULL,
	[NroInscricaoEstadual] [numeric](13, 0) NULL,
	[NroInscricaoMunicipal] [numeric](13, 0) NULL,
	[NroRegistroAnvisa] [numeric](10, 0) NULL,
	[StaQualificacao_APalc] [tinyint] NULL,
	[StaQualificacao_ADiqc] [tinyint] NULL,
	[StaQualificacao_AOna] [tinyint] NULL,
	[StaQualificacao_ACba] [tinyint] NULL,
	[StaQualificacao_AIqg] [tinyint] NULL,
	[StaQualificacao_N] [tinyint] NULL,
	[StaQualificacao_P] [tinyint] NULL,
	[StaQualificacao_R] [tinyint] NULL,
	[StaQualificacao_E] [tinyint] NULL,
	[StaQualificacao_Q] [tinyint] NULL,
	[CodClassificacao] [numeric](1, 0) NULL,
	[TpoContratualizacao] [varchar](1) NULL,
	[CodANSOperadoraIntermediaria] [varchar](6) NULL,
	[TpoDisponibilidadeServico] [varchar](1) NULL,
	[NroCNES] [varchar](15) NULL,
	[StaNecroterio] [tinyint] NULL,
	[StaAmbulancia] [tinyint] NULL,
	[StaHospitalDia] [tinyint] NULL,
	[StaTransplantes] [tinyint] NULL,
 CONSTRAINT [PK_D024_CONTRATADO] PRIMARY KEY NONCLUSTERED 
(
	[NroContratado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[D024_CONTRATADO] ADD  CONSTRAINT [DF__D024_CONT__StaQu__64B2867E]  DEFAULT ((0)) FOR [StaQualificacao_APalc]
GO
ALTER TABLE [dbo].[D024_CONTRATADO] ADD  CONSTRAINT [DF__D024_CONT__StaQu__65A6AAB7]  DEFAULT ((0)) FOR [StaQualificacao_ADiqc]
GO
ALTER TABLE [dbo].[D024_CONTRATADO] ADD  CONSTRAINT [DF__D024_CONT__StaQu__669ACEF0]  DEFAULT ((0)) FOR [StaQualificacao_AOna]
GO
ALTER TABLE [dbo].[D024_CONTRATADO] ADD  CONSTRAINT [DF__D024_CONT__StaQu__678EF329]  DEFAULT ((0)) FOR [StaQualificacao_ACba]
GO
ALTER TABLE [dbo].[D024_CONTRATADO] ADD  CONSTRAINT [DF__D024_CONT__StaQu__68831762]  DEFAULT ((0)) FOR [StaQualificacao_AIqg]
GO
ALTER TABLE [dbo].[D024_CONTRATADO] ADD  CONSTRAINT [DF__D024_CONT__StaQu__69773B9B]  DEFAULT ((0)) FOR [StaQualificacao_N]
GO
ALTER TABLE [dbo].[D024_CONTRATADO] ADD  CONSTRAINT [DF__D024_CONT__StaQu__6A6B5FD4]  DEFAULT ((0)) FOR [StaQualificacao_P]
GO
ALTER TABLE [dbo].[D024_CONTRATADO] ADD  CONSTRAINT [DF__D024_CONT__StaQu__6B5F840D]  DEFAULT ((0)) FOR [StaQualificacao_R]
GO
ALTER TABLE [dbo].[D024_CONTRATADO] ADD  CONSTRAINT [DF__D024_CONT__StaQu__6C53A846]  DEFAULT ((0)) FOR [StaQualificacao_E]
GO
ALTER TABLE [dbo].[D024_CONTRATADO] ADD  CONSTRAINT [DF__D024_CONT__StaQu__6D47CC7F]  DEFAULT ((0)) FOR [StaQualificacao_Q]
GO
ALTER TABLE [dbo].[D024_CONTRATADO] ADD  CONSTRAINT [DF_D024_CONTRATADO_StaNecroterio]  DEFAULT ((0)) FOR [StaNecroterio]
GO
ALTER TABLE [dbo].[D024_CONTRATADO] ADD  CONSTRAINT [DF_D024_CONTRATADO_StaAmbulancia]  DEFAULT ((0)) FOR [StaAmbulancia]
GO
ALTER TABLE [dbo].[D024_CONTRATADO] ADD  CONSTRAINT [DF_D024_CONTRATADO_StaHospitalDia]  DEFAULT ((0)) FOR [StaHospitalDia]
GO
ALTER TABLE [dbo].[D024_CONTRATADO] ADD  CONSTRAINT [DF_D024_CONTRATADO_StaTransplantes]  DEFAULT ((0)) FOR [StaTransplantes]