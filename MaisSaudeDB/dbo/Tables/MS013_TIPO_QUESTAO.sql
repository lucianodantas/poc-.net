﻿CREATE TABLE [dbo].[MS013_TIPO_QUESTAO](
	[NroTpoQuestao] [smallint] NOT NULL IDENTITY(1,1),
	[DesTpoQuestao] [varchar](60) NOT NULL,
	[DtaIniValidade] [smalldatetime] NOT NULL,
	[DtaFimValidade] [smalldatetime] NULL,
	[DtaAtualizacao] [smalldatetime] NOT NULL,
	[UserId] [numeric](11, 0) NOT NULL,
 CONSTRAINT [PK_MS013_TIPO_QUESTAO] PRIMARY KEY CLUSTERED 
(
	[NroTpoQuestao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]