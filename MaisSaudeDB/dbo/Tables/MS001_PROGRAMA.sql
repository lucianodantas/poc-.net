﻿CREATE TABLE [dbo].[MS001_PROGRAMA](
	[NroPrograma] [int] NOT NULL IDENTITY(1,1),
	[NroArea] [tinyint] NOT NULL,
	[NmePrograma] [varchar](60) NOT NULL,
	[DesPrograma] [varchar](512) NULL,
	[NroTpoPrograma] [tinyint] NOT NULL,
	[StaPrimeiroAtendimento] [tinyint] NOT NULL,
	[DtaIniValidade] [smalldatetime] NULL,
	[DtaFimValidade] [smalldatetime] NULL,
	[DtaAtualizacao] [smalldatetime] NOT NULL,
	[UserId] [numeric](11, 0) NOT NULL,
 CONSTRAINT [PK_MS001_PROGRAMA] PRIMARY KEY CLUSTERED 
(
	[NroPrograma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MS001_PROGRAMA]  WITH CHECK ADD  CONSTRAINT [FK_MS001_PROGRAMA_MS067_TIPO_PROGRAMA] FOREIGN KEY([NroTpoPrograma])
REFERENCES [dbo].[MS067_TIPO_PROGRAMA] ([NroTpoPrograma])
GO

ALTER TABLE [dbo].[MS001_PROGRAMA] CHECK CONSTRAINT [FK_MS001_PROGRAMA_MS067_TIPO_PROGRAMA]
GO
ALTER TABLE [dbo].[MS001_PROGRAMA] ADD  DEFAULT ((0)) FOR [StaPrimeiroAtendimento]