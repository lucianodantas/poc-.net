﻿CREATE TABLE [dbo].[D044_CIDADE](
	[NroCidade] [int] NOT NULL,
	[NmeCidade] [varchar](50) NULL,
	[SglEstado] [varchar](2) NULL,
	[Cep] [int] NULL,
	[CodRegiao] [varchar](2) NULL,
	[NroPolo] [smallint] NULL,
	[DtaInibicao] [datetime] NULL,
	[DtaAtualizacao] [datetime] NULL,
	[UserId] [varchar](10) NULL,
	[NroCidadeIBGE] [int] NULL,
	[StaCapital] [tinyint] NULL,
 CONSTRAINT [PK_D044_CIDADE] PRIMARY KEY NONCLUSTERED 
(
	[NroCidade] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[D044_CIDADE] ADD  DEFAULT ((0)) FOR [StaCapital]