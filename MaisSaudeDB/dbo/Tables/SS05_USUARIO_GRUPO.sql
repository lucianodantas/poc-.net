﻿CREATE TABLE [dbo].[SS05_USUARIO_GRUPO](
	[NroCpf] [numeric](11, 0) NOT NULL,
	[NroGrupo] [int] NOT NULL,
	[DtaAtualizacao] [smalldatetime] NULL,
	[UserId] [numeric](11, 0) NULL,
	[DtaIniValidade] [smalldatetime] NOT NULL,
	[DtaFimValidade] [smalldatetime] NULL,
	[NroCpfSolicitante] [numeric](11, 0) NULL,
	[NroCpfAutorizador] [numeric](11, 0) NULL,
	[NroChamado] [int] NULL,
 CONSTRAINT [PK_SS05_USUARIO_GRUPO] PRIMARY KEY CLUSTERED 
(
	[NroCpf] ASC,
	[NroGrupo] ASC,
	[DtaIniValidade] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 98) ON [PRIMARY]
) ON [PRIMARY]