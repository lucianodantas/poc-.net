
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/16/2015 10:16:17
-- Generated from EDMX file: C:\Users\Luciano\Documents\Visual Studio 2013\Projects\MaisSaude\MaisSaudePersistencia\Data\GeapModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [MaisSaudeDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Usuario'
CREATE TABLE [dbo].[Usuario] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nome] nvarchar(max)  NOT NULL,
    [email] nvarchar(max)  NOT NULL,
    [senha] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Perfil'
CREATE TABLE [dbo].[Perfil] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nome] nvarchar(max)  NOT NULL,
    [descricao] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Funcionalidade'
CREATE TABLE [dbo].[Funcionalidade] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nome] nvarchar(max)  NOT NULL,
    [descricao] nvarchar(max)  NOT NULL,
    [endereco] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'UsuarioPerfil'
CREATE TABLE [dbo].[UsuarioPerfil] (
    [UsuarioPerfil_Perfil_Id] int  NOT NULL,
    [Perfis_Id] int  NOT NULL
);
GO

-- Creating table 'PerfilFuncionalidade'
CREATE TABLE [dbo].[PerfilFuncionalidade] (
    [PerfilFuncionalidade_Funcionalidade_Id] int  NOT NULL,
    [Funcionalidades_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Usuario'
ALTER TABLE [dbo].[Usuario]
ADD CONSTRAINT [PK_Usuario]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Perfil'
ALTER TABLE [dbo].[Perfil]
ADD CONSTRAINT [PK_Perfil]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Funcionalidade'
ALTER TABLE [dbo].[Funcionalidade]
ADD CONSTRAINT [PK_Funcionalidade]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [UsuarioPerfil_Perfil_Id], [Perfis_Id] in table 'UsuarioPerfil'
ALTER TABLE [dbo].[UsuarioPerfil]
ADD CONSTRAINT [PK_UsuarioPerfil]
    PRIMARY KEY CLUSTERED ([UsuarioPerfil_Perfil_Id], [Perfis_Id] ASC);
GO

-- Creating primary key on [PerfilFuncionalidade_Funcionalidade_Id], [Funcionalidades_Id] in table 'PerfilFuncionalidade'
ALTER TABLE [dbo].[PerfilFuncionalidade]
ADD CONSTRAINT [PK_PerfilFuncionalidade]
    PRIMARY KEY CLUSTERED ([PerfilFuncionalidade_Funcionalidade_Id], [Funcionalidades_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [UsuarioPerfil_Perfil_Id] in table 'UsuarioPerfil'
ALTER TABLE [dbo].[UsuarioPerfil]
ADD CONSTRAINT [FK_UsuarioPerfil_Usuario]
    FOREIGN KEY ([UsuarioPerfil_Perfil_Id])
    REFERENCES [dbo].[Usuario]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Perfis_Id] in table 'UsuarioPerfil'
ALTER TABLE [dbo].[UsuarioPerfil]
ADD CONSTRAINT [FK_UsuarioPerfil_Perfil]
    FOREIGN KEY ([Perfis_Id])
    REFERENCES [dbo].[Perfil]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UsuarioPerfil_Perfil'
CREATE INDEX [IX_FK_UsuarioPerfil_Perfil]
ON [dbo].[UsuarioPerfil]
    ([Perfis_Id]);
GO

-- Creating foreign key on [PerfilFuncionalidade_Funcionalidade_Id] in table 'PerfilFuncionalidade'
ALTER TABLE [dbo].[PerfilFuncionalidade]
ADD CONSTRAINT [FK_PerfilFuncionalidade_Perfil]
    FOREIGN KEY ([PerfilFuncionalidade_Funcionalidade_Id])
    REFERENCES [dbo].[Perfil]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Funcionalidades_Id] in table 'PerfilFuncionalidade'
ALTER TABLE [dbo].[PerfilFuncionalidade]
ADD CONSTRAINT [FK_PerfilFuncionalidade_Funcionalidade]
    FOREIGN KEY ([Funcionalidades_Id])
    REFERENCES [dbo].[Funcionalidade]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PerfilFuncionalidade_Funcionalidade'
CREATE INDEX [IX_FK_PerfilFuncionalidade_Funcionalidade]
ON [dbo].[PerfilFuncionalidade]
    ([Funcionalidades_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------