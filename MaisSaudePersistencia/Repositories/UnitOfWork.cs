﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MaisSaudePersistencia.Data;

namespace MaisSaudePersistencia.Repositories
{
    public class UnitOfWork : IDisposable
    {
        private GeapModelContainer context = new GeapModelContainer();
        private GenericRepository<Usuario> usuarioRepository;
        private GenericRepository<Perfil> perfilRepository;
        private GenericRepository<Funcionalidade> funcionalidadeRepository;
         
        public GenericRepository<Usuario> UsuarioRepository 
        {
            get
            {
                if (this.usuarioRepository == null)
                    this.usuarioRepository = new GenericRepository<Usuario>(context);
                return usuarioRepository;
            }
        }

        public GenericRepository<Perfil> PerfilRepository
        {
            get
            {
                if (this.perfilRepository == null)
                    this.perfilRepository = new GenericRepository<Perfil>(context);
                return perfilRepository;
            }
        }

        public GenericRepository<Funcionalidade> FuncionalidadeRepository
        {
            get
            {
                if (this.funcionalidadeRepository == null)
                    this.funcionalidadeRepository = new GenericRepository<Funcionalidade>(context);
                return funcionalidadeRepository;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}