﻿using MaisSaudePersistencia.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using LinqToQuerystring;

namespace MaisSaudePersistencia.Repositories
{
    public class GenericRepository<TEntity> where TEntity : class
    {
        internal GeapModelContainer context;
        internal DbSet<TEntity> dbSet;

        public GenericRepository(GeapModelContainer context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }

        public virtual IEnumerable<TEntity> Get(string queryString)
        {
            if (queryString == null)
            {
                queryString = "";
            }

            var query = dbSet.AsQueryable().LinqToQuerystring(queryString);
            
            return query.ToList(); 
        }
 
        public virtual int Count(string queryString)
        {
            if (queryString == null)
            {
                queryString = "";
            }

            var query = dbSet.AsQueryable().LinqToQuerystring(queryString);

            return query.Count();
        }


        public virtual TEntity GetByID(object id)
        {
            return dbSet.Find(id);
        }

        public virtual void Insert(TEntity entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            TEntity entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }
            dbSet.Remove(entityToDelete);
        }

        public virtual void Update(TEntity entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;
        }
    }
}