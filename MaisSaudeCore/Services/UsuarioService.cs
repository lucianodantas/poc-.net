﻿using MaisSaudeCore.ValueObject;
using MaisSaudePersistencia.Data;
using MaisSaudePersistencia.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaisSaudeCore.Services
{
    public class UsuarioService
    {
        private UnitOfWork _uow;

        public UsuarioService()
        {
            _uow = new UnitOfWork();
        }

        public Usuario Autenticar(string email, string senha)
        {
            Usuario usuario = null;
            
            IEnumerable<Usuario> usuarios = _uow.UsuarioRepository.Get(  
                "$filter=email eq '" + email + "' and senha eq '" + senha + "'");

            if (usuarios.Count() > 0)
            {
                usuario = usuarios.First();
                usuario.token = "123456";
                usuario.senha = null;
            }

            return usuario;
        }

        public ConsultaUsuarioVO GetUsuarios(string queryString, string page, string pageSize)
        {
            ConsultaUsuarioVO vo = new ConsultaUsuarioVO();

            vo.usuarios = _uow.UsuarioRepository.Get(
                queryString + "$skip=" + page + "$top=" + pageSize);
            vo.count = _uow.UsuarioRepository.Count(queryString);
             
            return vo;
        }


        public Usuario GetById(int id)
        {
            return _uow.UsuarioRepository.GetByID(id);
        }

        public Usuario Save(Usuario usuario)
        {
            //Verifica se ja existe um usuario com o mesmo email
            IEnumerable<Usuario> usuarios = _uow.UsuarioRepository.Get(
                "$filter=email eq '" + usuario.email + "'" );

            if (usuarios.Count() > 0)
            {
                return null;
            }


            _uow.UsuarioRepository.Insert(usuario);
            _uow.Save();
            return usuario;
        }

        public Usuario Update(Usuario usuario)
        {
            _uow.UsuarioRepository.Update(usuario);
            _uow.Save();
            return usuario;
        }

        public Usuario Delete(Usuario usuario)
        {
            _uow.UsuarioRepository.Delete(usuario);
            _uow.Save();
            return usuario;
        }
    }
}
