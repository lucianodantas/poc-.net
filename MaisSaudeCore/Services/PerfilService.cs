﻿using MaisSaudeCore.ValueObject;
using MaisSaudePersistencia.Data;
using MaisSaudePersistencia.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaisSaudeCore.Services
{
    public class PerfilService
    {
        private UnitOfWork _uow;

        public PerfilService()
        {
            _uow = new UnitOfWork();
        }

        public ConsultaPerfilVO GetPerfis(string queryString, string page, string pageSize)
        {
            ConsultaPerfilVO vo = new ConsultaPerfilVO();

            vo.perfis = _uow.PerfilRepository.Get(
                queryString + "$skip=" + page + "$top=" + pageSize);
            vo.count = _uow.PerfilRepository.Count(queryString);

            return vo;
        }


        public Perfil GetById(int id)
        {
            return _uow.PerfilRepository.GetByID(id);
        }

        public Perfil Save(Perfil perfil)
        {
            //Verifica se ja existe um perfil com o mesmo nome
            IEnumerable<Perfil> perfis = _uow.PerfilRepository.Get(
                "$filter=nome eq '" + perfil.nome + "'");

            if (perfis.Count() > 0)
            {
                return null;
            }

            _uow.PerfilRepository.Insert(perfil);
            _uow.Save();
            return perfil;
        }

        public Perfil Update(Perfil perfil)
        {
            _uow.PerfilRepository.Update(perfil);
            _uow.Save();
            return perfil;
        }

        public Perfil Delete(Perfil perfil)
        {
            _uow.PerfilRepository.Delete(perfil);
            _uow.Save();
            return perfil;
        }

    }
}
