﻿using MaisSaudePersistencia.Data;
using MaisSaudePersistencia.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaisSaudeCore.Services
{
    public class FuncionalidadeService
    {
        private UnitOfWork _uow;

        public FuncionalidadeService()
        {
            _uow = new UnitOfWork();
        }

        public IEnumerable<Funcionalidade> GetFuncionalidades(String queryString)
        {
            return _uow.FuncionalidadeRepository.Get(queryString);
        }


        public Funcionalidade GetById(int id)
        {
            return _uow.FuncionalidadeRepository.GetByID(id);
        }

        public Funcionalidade Save(Funcionalidade funcionalidade)
        {
            _uow.FuncionalidadeRepository.Insert(funcionalidade);
            _uow.Save();
            return funcionalidade;
        }

        public Funcionalidade Update(Funcionalidade funcionalidade)
        {
            _uow.FuncionalidadeRepository.Update(funcionalidade);
            _uow.Save();
            return funcionalidade;
        }

        public Funcionalidade Delete(Funcionalidade funcionalidade)
        {
            _uow.FuncionalidadeRepository.Delete(funcionalidade);
            _uow.Save();
            return funcionalidade;
        }

    }
}
