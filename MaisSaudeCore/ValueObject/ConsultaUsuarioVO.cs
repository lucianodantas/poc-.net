﻿using MaisSaudePersistencia.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaisSaudeCore.ValueObject
{
    public class ConsultaUsuarioVO
    {

        public int count { get; set; }

        public IEnumerable<Usuario> usuarios { get; set; }

    }
}
