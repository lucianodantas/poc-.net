﻿using MaisSaudePersistencia.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaisSaudeCore.ValueObject
{
    public class ConsultaPerfilVO
    {

        public int count { get; set; }

        public IEnumerable<Perfil> perfis { get; set; }

    }
}
