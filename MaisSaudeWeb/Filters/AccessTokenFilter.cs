﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Threading;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Collections;

namespace MaisSaudeWeb.Filters
{
    public class AccessTokenFilter : DelegatingHandler
    {
        private const string tokenValido = "123456";

        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            //var accessToken = request.Headers.GetCookies("token");
            IEnumerable<string> authToken = null;
            
            try
            {
                authToken = request.Headers.GetValues("AUTH_TOKEN");
            }
            catch (InvalidOperationException e)
            {
                var noAuthResponse = request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Authorization token required!");
                return noAuthResponse;
            }
           
            
            if (authToken == null)
            {
               
            }

            var tokenValue = authToken.ToList().ElementAt(0);
            
            //var token = _accessTokenRepository.FindById(tokenValue);
            if (tokenValido.CompareTo(tokenValue) != 0)
            {
                var noAuthResponse = request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Authorization token invalid!");
                return noAuthResponse;
            }

/*
            var user = new Usuario();

            var identity = new GenericIdentity(user.Usuario, "Basic");
            var principal = new GenericPrincipal(identity, null);
            Thread.CurrentPrincipal = principal;
*/

            return await base.SendAsync(request, cancellationToken);
        }
    }
}