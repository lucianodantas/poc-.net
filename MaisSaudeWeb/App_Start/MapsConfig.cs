﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace MaisSaudeWeb
{
    public static class MapsConfig
    {
        public static void Register()
        {
            /*
            Mapper.CreateMap<Livro, BookDto>()
                  .ForMember(b => b.Authors, m => m.MapFrom(a => a.Autores));

            Mapper.CreateMap<Autor, BookAuthorsDto>()
                  .ForMember(b => b.FullName, m => m.MapFrom(a => a.Nome + " " + a.Sobrenome))
                  .ForSourceMember(b => b.Livros, m => m.Ignore());

            Mapper.CreateMap<Livro, AuthorBooksDto>();
                  
            Mapper.CreateMap<Autor, AuthorDto>()
                  .ForMember(b => b.FullName, m => m.MapFrom(a => a.Nome + " " + a.Sobrenome))
                  .ForMember(a => a.Books, m => m.MapFrom(b => b.Livros))
                  .ForSourceMember(b => b.Livros, m => m.Ignore());

            Mapper.CreateMap<Critica, ReviewDto>()
                  .ForMember(r => r.BookId, m => m.MapFrom(s => s.LivroId));*/
        }

        public static T To<T>(this object source)
        {
            
            return Mapper.Map<T>(source);
        }

        public static IEnumerable<T> To<T>(this IEnumerable<object> source)
        {
            return Mapper.Map<IEnumerable<T>>(source);
        }
    }
}