﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using MaisSaudeWeb.Filters;

namespace MaisSaudeWeb
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            
            //config.MessageHandlers.Add(new ContadorAcessosFilter());
            config.MessageHandlers.Add(new AccessTokenFilter());
            //config.Filters.Add(new HttpsFilter());
            
            
            config.Filters.Add(new System.Web.Http.AuthorizeAttribute());
        }
    }
}
