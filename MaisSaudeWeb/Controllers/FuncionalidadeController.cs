﻿using MaisSaudeCore.Services;
using MaisSaudePersistencia.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MaisSaudeWeb.Controllers
{
    [AllowAnonymous]
    public class FuncionalidadeController : ApiController
    {
        private FuncionalidadeService funcionalidadeService;

        public FuncionalidadeController()
        {
            this.funcionalidadeService = new FuncionalidadeService();
        }
        [HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(funcionalidadeService.GetFuncionalidades(null));
        }
        
        [HttpGet]
        public IHttpActionResult Get(string q)
        {
            return Ok(funcionalidadeService.GetFuncionalidades(q));
        }

        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            Funcionalidade funcionalidade = funcionalidadeService.GetById(id);

            return Ok(funcionalidade);
        }

        [HttpPost]
        public IHttpActionResult Save([FromBody]Funcionalidade funcionalidade)
        {
            funcionalidade = funcionalidadeService.Save(funcionalidade);

            return Ok(funcionalidade);
        }

        [HttpPut]
        public IHttpActionResult Update(int id, [FromBody]Funcionalidade funcionalidade)
        {
            funcionalidade.Id = id;
            funcionalidadeService.Update(funcionalidade);

            return Ok(funcionalidade);
        } 

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            Funcionalidade funcionalidade = new Funcionalidade { Id = id };

            funcionalidadeService.Delete(funcionalidade);

            return Ok();
        }
    }
}
