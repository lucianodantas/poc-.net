﻿using MaisSaudeCore.Services;
using MaisSaudePersistencia.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Caching;
using System.Web.Http;

namespace MaisSaudeWeb.Controllers
{
    [AllowAnonymous]
    public class SessaoController : ApiController
    {
        private UsuarioService usuarioService;

        public SessaoController()
        { 
            this.usuarioService = new UsuarioService();
        }

        // GET api/session
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
       
        [HttpPost]
        public IHttpActionResult Autenticar([FromBody]Usuario usuario)
        {
            usuario = usuarioService.Autenticar(usuario.email, usuario.senha);

            return Ok(usuario);
        }

       
    }
}
