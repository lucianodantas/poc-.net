﻿using MaisSaudeCore.Services;
using MaisSaudePersistencia.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MaisSaudeWeb.Controllers
{
    [AllowAnonymous]
    public class PerfilController : ApiController
    {
        private PerfilService perfilService;

        public PerfilController()
        {
            this.perfilService = new PerfilService();
        }
        
        [HttpGet]
        public IHttpActionResult Get(string q, string page, string pageSize)
        {
            return Ok(perfilService.GetPerfis(q, page, pageSize));
        }

        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            Perfil perfil = perfilService.GetById(id);
           
            return Ok(perfil);
        }

        [HttpPost]
        public IHttpActionResult Save([FromBody]Perfil perfil)
        {
            perfil = perfilService.Save(perfil);

            return Ok(perfil);
        }

        [HttpPut]
        public IHttpActionResult Update(int id, [FromBody]Perfil perfil)
        {
            perfil.Id = id;
            perfilService.Update(perfil);

            return Ok(perfil);
        } 

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            Perfil perfil = new Perfil { Id = id };

            perfilService.Delete(perfil);

            return Ok();
        }
    }
}
