﻿using MaisSaudeCore.Services;
using MaisSaudePersistencia.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MaisSaudeWeb.Controllers
{
    [AllowAnonymous]
    public class UsuarioController : ApiController
    {
        private UsuarioService usuarioService;

        public UsuarioController()
        {
            this.usuarioService = new UsuarioService();
        }
      
          // GET api/atendimento
        [HttpGet]
        public IHttpActionResult Get(string q, string page, string pageSize)
        { 
            return Ok(usuarioService.GetUsuarios(q, page, pageSize));
        }

        // GET api/atendimento/5
        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            Usuario usuario = usuarioService.GetById(id);
           
            return Ok(usuario);
        }

        // POST api/atendimento
        [HttpPost]
        public IHttpActionResult Save([FromBody]Usuario usuario)
        {
            usuario = usuarioService.Save(usuario);

            return Ok(usuario);
        }

        // PUT api/atendimento/5
        [HttpPut]
        public IHttpActionResult Update(int id, [FromBody]Usuario usuario)
        {
            usuario.Id = id;
            usuarioService.Update(usuario);

            return Ok(usuario);
        } 

        // DELETE api/atendimento/5
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            Usuario usuario = new Usuario { Id = id };

            usuarioService.Delete(usuario);

            return Ok();
        }
    }
}
